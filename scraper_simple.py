#!/usr/bin/env python3
#
#    scraper_simple.py
#
# simple example of web scraper
#
# author:
#     Alicia González Martínez (Universität Hamburg)
#     https://gitlab.com/kabikaj
#     aliciagm85@gmail.com
#
# usage:
#   $ python scraper_simple.py
#
###############################################################################

# in python 2, urllib.error, urllib.error and urllib.parse are all stored under urllib2,
# so, e.g., instead of urllib.request.urlopen, we would have to call urllib2.urlopen

import sys
import urllib.error
import urllib.request
from bs4 import BeautifulSoup


if __name__ == '__main__':

    try:
        resp = urllib.request.urlopen('https://www.example.com')

    # documentation for errors: https://docs.python.org/3.1/library/urllib.error.html
    except urllib.error.HTTPError as e:
        print('ERROR requesting url: %s' % e, file=sys.stderr)
        sys.exit(1)

    # More info on parsers: https://www.crummy.com/software/BeautifulSoup/bs4/doc/#installing-a-parser
    shorba = BeautifulSoup(resp, 'html.parser')

    #print(shorba)
    print(shorba.body.text)

    # check documentation for BeautifulSoup, https://www.crummy.com/software/BeautifulSoup/bs4/doc/
