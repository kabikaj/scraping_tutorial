#!/usr/bin/env python3
#
#    scraper_retrieve.py
#
# example of web scraper, getting a rar file
#
# author:
#     Alicia González Martínez (Universität Hamburg)
#     https://gitlab.com/kabikaj
#     aliciagm85@gmail.com
#
# usage:
#   $ python scraper_retrieve.py
#
###############################################################################

import sys
import urllib.error
import urllib.request
import urllib.parse
from bs4 import BeautifulSoup

URL = 'http://shamela.ws/index.php/book/149015'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'

HEADERS = {'User-Agent' : USER_AGENT}


if __name__ == '__main__':

    req = urllib.request.Request(URL, data=None, headers=HEADERS)

    try:
        resp = urllib.request.urlopen(req)

    except urllib.error.HTTPError as e:
        print('ERROR requesting url: %s' % e, file=sys.stderr)
        sys.exit(1)

    shorba = BeautifulSoup(resp, 'html.parser')

    # <a href="http://shamela.ws/books/1490/149016.rar"><img width="64" alt="حمل الكتاب" src="/files/img/front/bok.png" title="حمل الكتاب" /></a>

    links = (tag['href'] for tag in shorba.findAll('a') if tag.has_attr('href'))
    rar = [link for link in links if link.endswith('.rar')][0]

    print('Found... ', rar)

    urllib.request.urlretrieve(rar, 'data.rar')
