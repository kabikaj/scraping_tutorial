#!/usr/bin/env python3
#
#    scraper_requestObj.py
#
# example of web scraper, using a Request object instead of the url
# string directly
#
# author:
#     Alicia González Martínez (Universität Hamburg)
#     https://gitlab.com/kabikaj
#     aliciagm85@gmail.com
#
# usage:
#   $ python scraper_requestObj.py
#
#####################################################################

import sys
import urllib.error
import urllib.request
from bs4 import BeautifulSoup

URL = 'https://www.whatismybrowser.com'

# this identifies the devise you are using to access a web, check possible user agents at: https://developers.whatismybrowser.com/
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'

# additional information sent with the request, https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
HEADERS = {'User-Agent' : USER_AGENT}


if __name__ == '__main__':

    # documentation for Request: https://docs.python.org/3/library/urllib.request.html#module-urllib.request
    req = urllib.request.Request(URL, data=None, headers=HEADERS)

    try:
        # urlopen receives an arg url which is either a string or a Request object
        #resp = urllib.request.urlopen(URL)  # My user agent will be "Python-urllib/3.7"
        resp = urllib.request.urlopen(req)  # My user agent will be "Mozilla/5.0 (...)"

    except urllib.error.HTTPError as e:
        print('ERROR requesting url: %s' % e, file=sys.stderr)
        sys.exit(1)

    shorba = BeautifulSoup(resp, 'html.parser')

    # we want to extract the text in this tag from the html object:
    #<div class="user-agent">
    #    <h3>Your web browser's user agent:</h3>
    #    <a href="https://developers.whatismybrowser.com/(...)" title="Your User Agent">Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0</a>
    #</div>
    # <a href="/detect/what-is-my-ip-address" title="Your IP Address">87.123.201.106</a>
    my_user_agent = shorba.find('div', {'class':'user-agent'}).text
    print(my_user_agent)

    # you can use the inspector on your browser to see where is the data you want to get
