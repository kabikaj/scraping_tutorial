#!/usr/bin/env python3
#
#    scraper_senddata.py
#
# example of web scraper, sending specific data in our request
#
# author:
#     Alicia González Martínez (Universität Hamburg)
#     https://gitlab.com/kabikaj
#     aliciagm85@gmail.com
#
# usage:
#   $ python scraper_senddata.py
#
###############################################################################

import sys
import urllib.error
import urllib.request
import urllib.parse  # Note that we added an import!
from bs4 import BeautifulSoup

URL = 'https://www.youtube.com/watch'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'

HEADERS = {'User-Agent' : USER_AGENT}


PARAMS = {   
    'v': 'oZWNGq70Oyo'
}


if __name__ == '__main__':

    # this is the url we want to get https://www.youtube.com/watch?v=oZWNGq70Oyo

    query = urllib.parse.urlencode(PARAMS)
    url = URL + '?' + query
    
    # we can either make the request with GET parameters ...
    #req = urllib.request.Request(url, data=None, headers=HEADERS)

    # ... or with POST parameters
    data = query.encode('utf-8')
    req = urllib.request.Request(url, data=data, headers=HEADERS)

    try:
        resp = urllib.request.urlopen(req)

    except urllib.error.HTTPError as e:
        print('ERROR requesting url: %s' % e, file=sys.stderr)
        sys.exit(1)

    shorba = BeautifulSoup(resp, 'html.parser')

    print(shorba)

