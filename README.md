
# Web scraping with python

Tutorial for IDHN May 24th 2019

### Code examples

1. scraper_simple.py
2. scraper_requestObj.py
3. scraper_senddata.py
4. scraper_retrieve.py


#### Contact

Alicia González Martínez , *aliciagm85 at gmail dot com*
